/*
 The server package provides utilities to expose a RESTful API via HTTP.
 A collection of services will be made. Each service will control a domain of the application. This ranges from data
 access to a model collection, executing business logic, generating reports/output etc.

 Each service should be self contained, define it's own routes and work independently. When a server is deployed, a 
 series of services are added to it. As requests come in, all requests will be routed to the corresponding service.
*/
package server

import (
	"github.com/gorilla/mux"
	"gxs3/godfather/model"
	"net/http"
	"runtime/debug"
)

/*
 Stub struct used to create a default http handler. This handler will return BAD_REQUEST when the request made does not 
 match any of the routes set up in service routers
*/
type notFoundHandler struct{}

/*
 Base service. Used by service implementations for embedding, and making their creation simpler.
*/
type BaseService struct {
	pattern string      //Global URI pattern used to reach the service.
	router  *mux.Router //Router used to route calls to different service end points
}

const (
	GET    = "GET"    //HTTP GET Method
	POST   = "POST"   //HTTP POST Method
	PUT    = "PUT"    //HTTP PUT Method
	DELETE = "DELETE" //HTTP DELETE Method

	OK                    = 200 //HTTP OK Status Code
	CREATED               = 201 //HTTP CREATED Status Code
	BAD_REQUEST           = 400 //HTTP BAD_REQUEST Status Code
	UNAUTHORIZED          = 401 //HTTP UNAUTHORIZED Status Code
	FORBIDDEN             = 403 //HTTP FORBIDDEN Status Code
	NOT_FOUND             = 404 //HTTP NOT_FOUND Status Code
	METHOD_NOT_ALLOWED    = 405 //HTTP METHOD_NOT_ALLOWED Status Code
	REQUEST_TIMEOUT       = 408 //HTTP REQUEST_TIMEOUT Status Code
	CONFLICT              = 409 //HTTP CONFLICT Status Code
	INTERNAL_SERVER_ERROR = 500 //HTTP INTERNAL_SERVER_ERROR Status Code
	NOT_IMPLEMENTED       = 501 //HTTP NOT_IMPLEMENTED Status Code
	SERVICE_UNAVAILABLE   = 503 //HTTP SERVICE_UNAVAILABLE Status Code

	JSON  = "application/json" //HTTP standard JSON content type. Will be used when serializing complex objects
	HTML  = "text/html"        //HTTP standard HTML content type
	PLAIN = "text/plain"       //HTTP standard plain text content type. Will be used as a defualt, when returning simple results.

	STATUS        = "Status"        //HTTP STATUS Header
	CONTENT_TYPE  = "Content-Type"  //HTTP CONTENT_TYPE Header
	CACHE_CONTROL = "Cache-Control" //HTTP CACHE_CONTROL Header
	PRAGMA        = "Pragma"        //HTTP PRAGMA Header
	EXPIRES       = "Expires"       //HTTP EXPIRES Header

	INVALID_ID      = "The provided value was not a valid model ID"  //Error indicating a bad model ID
	INVALID_PAYLOAD = "Payload could not be parsed as a valid model" //Error indicating a bad model serialization
	EMPTY_PAYLOAD   = "Payload was empty"                            //Error indicating that the model string sent was empty

	MODEL = "model" //Standard key used to identify the model in a server message
	LINKS = "links" //Standard key used to identify the link collection in a server message

	ID_PATTERN = "/{id:[0-9]+}" //Standard pattern used to route requests to end points that look for model IDs
	ID         = "id"           //Standard identifier for the ID in request URI patterns
)

/*
 Creates a new base service. Requires a router to be provided
*/
func NewBaseService(router *mux.Router) *BaseService {
	if router == nil {
		panic(model.NIL_ARGUMENT)
	}

	var s = new(BaseService)
	s.router = router
	s.router.NotFoundHandler = new(notFoundHandler)

	return s
}

/*
 Utility function that serves the purpose of providing exatch matches on request URIs. Useful in certain routing situations.
 The library being used for request routing (github.com/gorilla/mux) takes a liberal approach when routing calls, between
 parent and child elements. This assures that the pattern only matches the parent and nothing else.
*/
func ExactMatch(uri string) mux.MatcherFunc {
	return func(req *http.Request, match *mux.RouteMatch) bool {
		return req.RequestURI == uri
	}
}

/*
 Utility used to avoid panics from killing the HTTP Server or one of its services. Any time a panic is raised,
 this function will recover, and send a stack trace to the client.
*/
func Recover(writer http.ResponseWriter) {
	if r := recover(); r != nil {
		writer.WriteHeader(INTERNAL_SERVER_ERROR)

		err, ok := r.(error)
		if ok {
			writer.Write([]byte(err.Error()))
			writer.Write([]byte("\n"))
			writer.Write(debug.Stack())
		}
	}
}

/*
 Utility function that converts a link object to a flat representation in a map. Useful when serializing models to be
 sent down the wire
*/
func toMap(link *model.Link) map[string]string {
	var data = make(map[string]string)

	data["rel"] = link.Rel()
	data["uri"] = link.Uri()
	data["method"] = link.Method()
	data["contentType"] = link.ContentType()

	return data
}

/*
 Used to create a default http handler. This handler will return BAD_REQUEST when the request made does not 
 match any of the routes set up in service routers
*/
func (nfh *notFoundHandler) ServeHTTP(writer http.ResponseWriter, req *http.Request) {
	http.Error(writer, "No end point matches the request", BAD_REQUEST)
}
