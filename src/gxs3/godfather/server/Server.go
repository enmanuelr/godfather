package server

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gxs3/godfather/data"
	"gxs3/godfather/model"
	"io"
	"net/http"
	"os"
	"strings"
)

const (
	DB_DRIVER      = "db-driver"      //Database driver configuration key
	DB_DATA_SOURCE = "db-data-source" //Database data source configuration key
	DB_POOL_SIZE   = "db-pool-size"   //Database pool size configuration key
	DB_MAX_RETRIES = "db-max-retries" //Connection pool connection acquisition maximum retries configuration key
	DB_QUERY_FILES = "db-query-files" //Query files configuration key
	DB_TEMPLATE    = "db-template"    //Runtime context value where the SQL Template is placed
	DB_DICTIONARY  = "db-dictionary"  //Runtime context value where the query dictionary is placed
	HTTP_PORT      = "http-port"      //HTTP port to run the server on

	ROOT        = "GODFATHER_ROOT"                 //Environment variable expected to be set which indicates the root of the application
	CONFIG_PATH = "/resources/panacea-config.json" //Relative path where the config file is expected to be
)

/*
 Defines an interface all services must implement. When services are added to the server, this type is what binds them
 all.
*/
type Service interface {
	/*
	 Post creation initialization. Both the server and services are initially created. Afterwards, when the service is
	 added to the server, initialization may need to occur to make the service fully functional. This usually includes
	 looking up the server context, and retrieving global values, doing initial setup, etc.
	*/
	Init(context map[string]interface{})
}

/*
 Application server. Will hold a collection of services, and will perform initial routing of requests within them. Once
 the request is sent to the service, said service is responsible for internal routing to the final end point.
*/
type Server struct {
	services []Service              //Collection of services registered
	port     uint                   //Port where the HTTP server will run on
	router   *mux.Router            //Router used to route calls to services and end points
	context  map[string]interface{} //Config and runtime properties used to initialized server and services
}

/*
 Creates a new server
*/
func NewServer() *Server {
	var s = new(Server)
	var err error

	s.context, err = readContext()

	if err != nil {
		panic(err.Error())
	}

	err = s.buildContext()

	if err != nil {
		panic(err.Error())
	}

	s.services = make([]Service, 0)
	s.router = mux.NewRouter()
	http.Handle("/", s.router)

	return s
}

/*
 Adds a new service to the server.
*/
func (s *Server) Add(service Service) {
	if service == nil {
		panic(model.NIL_ARGUMENT)
	}

	s.services = append(s.services, service)
}

/*
 All services are initialized and the HTTP server is started
*/
func (s *Server) Start() error {
	for _, service := range s.services {
		service.Init(s.context)
	}

	var port = fmt.Sprintf(":%d", uint(s.context[HTTP_PORT].(float64)))
	fmt.Printf("Server Started - %s\n", port)
	http.ListenAndServe(port, nil)
	return nil
}

/*
 Returns the router being used by the server
*/
func (s *Server) Router() *mux.Router {
	return s.router
}

/*
 Reads the context from the default configuration file, and places it on the server
*/
func readContext() (map[string]interface{}, error) {
	var root = os.Getenv(ROOT)
	root = strings.TrimSpace(root)

	if len(root) < 1 {
		panic("Environment variable " + ROOT + " has not been set. Please set this to the app root.")
	}

	var file, err = os.Open(root + CONFIG_PATH)

	if err != nil {
		return nil, err
	}

	defer file.Close()
	var reader = bufio.NewReader(file)
	var buffer = make([]byte, 0)

	for {
		line, err := reader.ReadBytes('\n')

		if err != nil {
			if err == io.EOF {
				break
			}

			return nil, err
		}

		var lineStr = strings.TrimSpace(string(line))

		if strings.HasPrefix(lineStr, "//") || strings.HasPrefix(lineStr, "#") {
			continue
		}

		buffer = append(buffer, line...)
	}

	var raw interface{}
	err = json.Unmarshal(buffer, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	return job, nil
}

/*
 Reads configuration in context, creates run time propeties based on them, and places them in the context
 as well.
*/
func (s *Server) buildContext() error {
	var driver = s.context[DB_DRIVER].(string)
	var source = s.context[DB_DATA_SOURCE].(string)
	var size = uint(s.context[DB_POOL_SIZE].(float64))

	var pool, err = data.NewConnectionPool(driver, source, size)

	if err != nil {
		return err
	}

	template, err := data.NewSqlTemplate(pool)

	if err != nil {
		return err
	}

	s.context[DB_TEMPLATE] = template

	var queryFiles = s.context[DB_QUERY_FILES].([]interface{})
	var dictionary = make(map[string]string)

	for _, fileRaw := range queryFiles {
		var file, ok = fileRaw.(string)

		if !ok {
			panic("Unexpected values in the 'db-query-files' config property")
		}

		pack, err := data.ReadQueryFile(file)

		if err != nil {
			return err
		}

		for key, query := range pack {
			dictionary[key] = query
		}
	}

	s.context[DB_DICTIONARY] = dictionary

	return nil
}
