package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"gxs3/godfather/data"
	"gxs3/godfather/model"
	"net/http"
	"strconv"
	"strings"
)

const (
	PRIVILEGE_COLLECTION = "/priv" //Privilege service global route

	PRIV_GRANT  = "/rels/gf.priv-grant"  //Rel for link that grants a privilege
	PRIV_REVOKE = "/rels/gf.priv-revoke" //Rel from link that revokes a privilege

	MODULE    = "module"   //Identifies module parameters in requests
	NOT_GROUP = "notgroup" //Identifies "not group" parameters in requests
)

/*
 Service implementation that handles access to the privilege collection.
 The privilege collection is almost exclusively read-only. Privileges are created when the system is created. They are
 closely tied to application logic, making it complicated to generate them dynamically. The only write operations supported
 by this service are the granting and revoking privileges to/from groups.
*/
type PrivilegeService struct {
	*BaseService
	dao *data.PrivilegeDao
}

/*
 Creates a new privilege service.
*/
func NewPrivilegeService(router *mux.Router) *PrivilegeService {
	var ps = new(PrivilegeService)
	ps.BaseService = NewBaseService(router)
	var subrouter = router.PathPrefix(PRIVILEGE_COLLECTION).Subrouter()

	var get = func(writer http.ResponseWriter, req *http.Request) { ps.get(writer, req) }
	var getAll = func(writer http.ResponseWriter, req *http.Request) { ps.getAll(writer, req) }
	var getByModule = func(writer http.ResponseWriter, req *http.Request) { ps.getByModule(writer, req) }
	var getGroupGranted = func(writer http.ResponseWriter, req *http.Request) { ps.getGroupGranted(writer, req) }
	var getGroupNotGranted = func(writer http.ResponseWriter, req *http.Request) { ps.getGroupNotGranted(writer, req) }
	var getUserGranted = func(writer http.ResponseWriter, req *http.Request) { ps.getUserGranted(writer, req) }
	var grant = func(writer http.ResponseWriter, req *http.Request) { ps.grant(writer, req) }
	var revoke = func(writer http.ResponseWriter, req *http.Request) { ps.revoke(writer, req) }

	subrouter.Methods(GET).Path(ID_PATTERN).HandlerFunc(get)
	router.Methods(GET).Path(PRIVILEGE_COLLECTION).Queries(MODULE, "").HandlerFunc(getByModule)
	router.Methods(GET).Path(PRIVILEGE_COLLECTION).Queries(GROUP, "").HandlerFunc(getGroupGranted)
	router.Methods(GET).Path(PRIVILEGE_COLLECTION).Queries(NOT_GROUP, "").HandlerFunc(getGroupNotGranted)
	router.Methods(GET).Path(PRIVILEGE_COLLECTION).Queries(USER, "").HandlerFunc(getUserGranted)
	router.Methods(GET).MatcherFunc(ExactMatch(PRIVILEGE_COLLECTION)).HandlerFunc(getAll)
	subrouter.Methods(POST).Path(ID_PATTERN).Queries(GROUP, "").HandlerFunc(grant)
	subrouter.Methods(DELETE).Path(ID_PATTERN).Queries(GROUP, "").HandlerFunc(revoke)

	return ps
}

/*
 Service initialization
*/
func (ps *PrivilegeService) Init(context map[string]interface{}) {
	var template = context[DB_TEMPLATE].(*data.SqlTemplate)
	var dictionary = context[DB_DICTIONARY].(map[string]string)
	var err error
	ps.dao, err = data.NewPrivilegeDao(template, dictionary)

	if err != nil {
		panic(err.Error())
	}
}

/*
 Retrieves the privilege identified by the provided ID.
 Method: GET - URI: /priv/{id}
*/
func (ps *PrivilegeService) get(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var vars = mux.Vars(req)
	var id, err = strconv.ParseUint(vars[ID], 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	priv, err := ps.dao.Get(uint(id))

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if priv == nil {
		writer.WriteHeader(NOT_FOUND)
		return
	}

	encoded, err := ps.encode(priv)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.Write(encoded)
}

/*
 Retrieves all existing privileges
 Method: GET - URI: /priv
*/
func (ps *PrivilegeService) getAll(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var privs, err = ps.dao.GetAll()

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if len(privs) < 1 {
		writer.WriteHeader(NOT_FOUND)
		return
	}

	encoded, err := ps.encodeSlice(privs)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.Write(encoded)
}

/*
 Retrieves all privileges which belong to the given module
 Method: GET - URI: /priv?module={module}
*/
func (ps *PrivilegeService) getByModule(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var module = req.FormValue(MODULE)

	if len(module) < 1 {
		http.Error(writer, model.EMPTY_STRING, BAD_REQUEST)
		return
	}

	privs, err := ps.dao.GetByModule(module)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if len(privs) < 1 {
		writer.WriteHeader(NOT_FOUND)
		return
	}

	encoded, err := ps.encodeSlice(privs)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.Write(encoded)
}

/*
 Retrieves all privileges which have been granted to a group
 Method: GET - URI: /priv?group={id-group}
*/
func (ps *PrivilegeService) getGroupGranted(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var idGroup, err = strconv.ParseUint(req.FormValue(GROUP), 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	privs, err := ps.dao.GetGroupGranted(uint(idGroup))

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if len(privs) < 1 {
		writer.WriteHeader(NOT_FOUND)
		return
	}

	encoded, err := ps.encodeSlice(privs)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.Write(encoded)
}

/*
 Retrieves all privileges which have NOT been granted to a group
 Method: GET - URI: /priv?notgroup={id-group}
*/
func (ps *PrivilegeService) getGroupNotGranted(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var idGroup, err = strconv.ParseUint(req.FormValue(NOT_GROUP), 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	privs, err := ps.dao.GetGroupNotGranted(uint(idGroup))

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if len(privs) < 1 {
		writer.WriteHeader(NOT_FOUND)
		return
	}

	encoded, err := ps.encodeSlice(privs)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.Write(encoded)
}

/*
 Retrieves all privileges which have been granted to a user. Privileges are granted to the user indirectly. The user
 inherits all privileges which have been granted to the group(s) he/she is a member of.
 Method: GET - URI: /priv?user={id-user}
*/
func (ps *PrivilegeService) getUserGranted(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var idUser, err = strconv.ParseUint(req.FormValue(USER), 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	privs, err := ps.dao.GetUserGranted(uint(idUser))

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if len(privs) < 1 {
		writer.WriteHeader(NOT_FOUND)
		return
	}

	encoded, err := ps.encodeSlice(privs)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.Write(encoded)
}

/*
 Grant a privilege to a group
 Method: POST - URI: /priv/{id-priv}?group={id-group}
*/
func (ps *PrivilegeService) grant(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var vars = mux.Vars(req)
	var idPriv, err = strconv.ParseUint(vars[ID], 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	idGroup, err := strconv.ParseUint(req.FormValue(GROUP), 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	records, err := ps.dao.Grant(uint(idPriv), uint(idGroup))

	if err != nil {
		if err == data.CONSTRAINT {
			http.Error(writer, err.Error(), CONFLICT)
		} else {
			http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		}

		return
	}

	if records < 1 {
		http.Error(writer, "Can't grant privilege. No privileges/groups matched the corresponding IDs", NOT_FOUND)
		return
	}

	writer.WriteHeader(OK)
}

/*
 Revoke a privilege from a group
 Method: DELETE - URI: /priv/{id-priv}?group={id-group}
*/
func (ps *PrivilegeService) revoke(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var vars = mux.Vars(req)
	var idPriv, err = strconv.ParseUint(vars[ID], 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	idGroup, err := strconv.ParseUint(req.FormValue(GROUP), 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	records, err := ps.dao.Revoke(uint(idPriv), uint(idGroup))

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if records < 1 {
		http.Error(writer, "Can't revoke privilege. No privileges/groups matched the corresponding IDs", 209)
		return
	}

	writer.WriteHeader(OK)
}

/*
 Encodes a privilege to a serialized representation for transport. Adds REST links.
*/
func (ps *PrivilegeService) encode(priv *model.Privilege) ([]byte, error) {
	var job = make(map[string]interface{})
	var obj = make(map[string]interface{})
	var links = make([]map[string]string, 2)
	var linkObjs = make([]*model.Link, 2)

	job[MODEL] = obj
	job[LINKS] = links
	obj["id"] = priv.Id()
	obj["module"] = priv.Module()
	obj["key"] = priv.Key()
	obj["name"] = priv.Name()
	obj["description"] = priv.Description()

	var resource = fmt.Sprintf("%s%s%d", PRIVILEGE_COLLECTION, "/", priv.Id())
	linkObjs[0], _ = model.NewLink(PLAIN, PRIV_GRANT, POST, fmt.Sprintf("%s%s", resource, "?group="))
	linkObjs[1], _ = model.NewLink(PLAIN, PRIV_REVOKE, DELETE, fmt.Sprintf("%s%s", resource, "?revoke="))

	for cont, link := range linkObjs {
		links[cont] = toMap(link)
	}

	encoded, err := json.Marshal(job)

	return encoded, err
}

/*
 Encodes a collection of privileges to a serialized representation for transport. Adds REST links to each privilege.
*/
func (ps *PrivilegeService) encodeSlice(privs []*model.Privilege) ([]byte, error) {
	var list = make([]string, 1)
	list[0] = "{\n"

	for _, priv := range privs {
		var resource = fmt.Sprintf(`"%s%s%d"`, PRIVILEGE_COLLECTION, "/", priv.Id())
		var encoded, err = ps.encode(priv)

		if err != nil {
			return nil, err
		}

		list = append(list, resource, ":", string(encoded), ",\n")
	}

	list[len(list)-1] = "\n}\n"
	var result = strings.Join(list, "")

	return []byte(result), nil
}

/*
 Decodes a privilege from a serialized representation into a model object.
*/
func (ps *PrivilegeService) decode(message []byte) (*model.Privilege, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 5)
	var id float64
	var module, key, name, description string

	id, ok[0] = job["id"].(float64)
	module, ok[1] = job["module"].(string)
	key, ok[2] = job["key"].(string)
	name, ok[3] = job["name"].(string)
	description, ok[4] = job["description"].(string)

	for _, val := range ok {
		if !val {
			return nil, errors.New(INVALID_PAYLOAD)
		}
	}

	priv, err := model.NewPrivilege(
		uint(id),
		module,
		key,
		name,
		description,
	)

	return priv, err
}
