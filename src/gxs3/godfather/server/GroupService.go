package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"gxs3/godfather/data"
	"gxs3/godfather/model"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

/*
 Service implementation that handles access to the group model collection
*/
type GroupService struct {
	*BaseService
	dao *data.GroupDao
}

const (
	GROUP_COLLECTION = "/group" //Group collection global route.

	GROUP_USERS       = "/rels/gf.group-users"       //Rel for link that retrieves users that belong to a group
	GROUP_OTHERS      = "/rels/gf.group-others"      //Rel tor link that retrieves users that *DON'T* belong to a group 
	GROUP_ADD_USER    = "/rels/gf.group-add-user"    //Rel for link that adds a user to a group
	GROUP_REMOVE_USER = "/rels/gf.group-remove-user" //Rel for link that removes user from a group
	GROUP_UPDATE      = "/rels/gf.group-update"      //Rel for link that updates a group
	GROUP_REMOVE      = "/rels/gf.group-remove"      //Rel for link that removes a group

	USER = "user" //Identifies user parameters sent in requests
)

/*
 Creates a new group service.
*/
func NewGroupService(router *mux.Router) *GroupService {
	var gs = new(GroupService)
	gs.BaseService = NewBaseService(router)
	var subrouter = router.PathPrefix(GROUP_COLLECTION).Subrouter()

	var get = func(writer http.ResponseWriter, req *http.Request) { gs.get(writer, req) }
	var getUser = func(writer http.ResponseWriter, req *http.Request) { gs.getByUser(writer, req) }
	var getAll = func(writer http.ResponseWriter, req *http.Request) { gs.getAll(writer, req) }
	var add = func(writer http.ResponseWriter, req *http.Request) { gs.add(writer, req) }
	var update = func(writer http.ResponseWriter, req *http.Request) { gs.update(writer, req) }
	var addUser = func(writer http.ResponseWriter, req *http.Request) { gs.addUser(writer, req) }
	var removeUser = func(writer http.ResponseWriter, req *http.Request) { gs.removeUser(writer, req) }
	var remove = func(writer http.ResponseWriter, req *http.Request) { gs.remove(writer, req) }

	subrouter.Methods(GET).Path(ID_PATTERN).HandlerFunc(get)
	subrouter.Methods(GET).Queries(USER, "").HandlerFunc(getUser)
	router.Methods(GET).MatcherFunc(ExactMatch(GROUP_COLLECTION)).HandlerFunc(getAll)
	router.Methods(POST).MatcherFunc(ExactMatch(GROUP_COLLECTION)).HandlerFunc(add)
	subrouter.Methods(PUT).Path(ID_PATTERN).HandlerFunc(update)
	subrouter.Methods(POST).Path(ID_PATTERN).Queries(USER, "").HandlerFunc(addUser)
	subrouter.Methods(DELETE).Path(ID_PATTERN).Queries(USER, "").HandlerFunc(removeUser)
	subrouter.Methods(DELETE).Path(ID_PATTERN).HandlerFunc(remove)

	return gs
}

/*
 Service initializtion.
*/
func (gs *GroupService) Init(context map[string]interface{}) {
	var template = context[DB_TEMPLATE].(*data.SqlTemplate)
	var dictionary = context[DB_DICTIONARY].(map[string]string)
	var err error
	gs.dao, err = data.NewGroupDao(template, dictionary)

	if err != nil {
		panic(err.Error())
	}
}

/*
 Retrieves a group identified by a given ID.
 Method: GET - URI: /group/{id}
*/
func (gs *GroupService) get(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var vars = mux.Vars(req)
	var id, err = strconv.ParseUint(vars[ID], 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	group, err := gs.dao.Get(uint(id))

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if group == nil {
		writer.WriteHeader(NOT_FOUND)
		return
	}

	encoded, err := gs.encode(group)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.Write(encoded)
}

/*
 Retrieves all groups to which a user is member of.
 Method: GET - URI: /group?user={id-user}
*/
func (gs *GroupService) getByUser(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var vars = mux.Vars(req)
	var idUser, err = strconv.ParseUint(vars[USER], 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	groups, err := gs.dao.GetByUser(uint(idUser))

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if len(groups) < 1 {
		writer.WriteHeader(NOT_FOUND)
		return
	}

	encoded, err := gs.encodeSlice(groups)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.Write(encoded)
}

/*
 Retrieves all existing groups.
 Method: GET - URI: /group
*/
func (gs *GroupService) getAll(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var groups, err = gs.dao.GetAll()

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if len(groups) < 1 {
		writer.WriteHeader(NOT_FOUND)
		return
	}

	encoded, err := gs.encodeSlice(groups)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.Write(encoded)
}

/*
 Adds a new group to the collection. Group model is expected in request payload.
 Method: POST - URI: /group
*/
func (gs *GroupService) add(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var groupStr, err = ioutil.ReadAll(req.Body)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	if len(groupStr) < 1 {
		http.Error(writer, EMPTY_PAYLOAD, BAD_REQUEST)
		return
	}

	group, err := gs.decode(groupStr)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	id, err := gs.dao.Add(group)

	if err != nil {
		if err == data.CONSTRAINT {
			http.Error(writer, err.Error(), CONFLICT)
		} else {
			http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		}

		return
	}

	group.SetId(id)
	groupStr, err = gs.encode(group)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.WriteHeader(CREATED)
	writer.Write(groupStr)
}

/*
 Updates the properties of an existing group. Model is expected in request payload.
 Method: PUT - URI: /group/{id}
*/
func (gs *GroupService) update(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var vars = mux.Vars(req)
	var id, err = strconv.ParseUint(vars[ID], 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	groupStr, err := ioutil.ReadAll(req.Body)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	if len(groupStr) < 1 {
		http.Error(writer, EMPTY_PAYLOAD, BAD_REQUEST)
		return
	}

	group, err := gs.decode(groupStr)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	if uint(id) != group.Id() {
		http.Error(writer, "Group ID does not match with ID in URI", BAD_REQUEST)
		return
	}

	records, err := gs.dao.Update(group)

	if err != nil {
		if err == data.CONSTRAINT {
			http.Error(writer, err.Error(), CONFLICT)
		} else {
			http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		}

		return
	}

	if records < 1 {
		http.Error(writer, "Can't update group. No group matches the provided ID", NOT_FOUND)
		return
	}

	groupStr, err = gs.encode(group)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.WriteHeader(OK)
	writer.Write(groupStr)
}

/*
 Makes a user member of the group
 Method: POST - URI: /group/{id-group}?user={id-user}
*/
func (gs *GroupService) addUser(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var vars = mux.Vars(req)
	var idGroup, err = strconv.ParseUint(vars[ID], 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	idUser, err := strconv.ParseUint(req.FormValue(USER), 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	records, err := gs.dao.AddUser(uint(idGroup), uint(idUser))

	if err != nil {
		if err == data.CONSTRAINT {
			http.Error(writer, err.Error(), CONFLICT)
		} else {
			http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		}

		return
	}

	if records < 1 {
		http.Error(writer, "Can't add user to group. No group and/or user matched the corresponding IDs", NOT_FOUND)
		return
	}

	writer.WriteHeader(OK)
}

/*
 Removes from a user membership to the group
 Method: DELETE - URI: /group/{id-group}?user={id-user}
*/
func (gs *GroupService) removeUser(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var vars = mux.Vars(req)
	var idGroup, err = strconv.ParseUint(vars[ID], 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	idUser, err := strconv.ParseUint(req.FormValue(USER), 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	records, err := gs.dao.RemoveUser(uint(idGroup), uint(idUser))

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if records < 1 {
		http.Error(writer, "Can't remove user from group. No group and/or user matched the corresponding IDs", NOT_FOUND)
		return
	}

	writer.WriteHeader(OK)
}

/*
 Removes a group from the collection
 Method: DELETE - URI: /group/{id}
*/
func (gs *GroupService) remove(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var vars = mux.Vars(req)
	var idGroup, err = strconv.ParseUint(vars[ID], 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	records, err := gs.dao.Remove(uint(idGroup))

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if records < 1 {
		http.Error(writer, "No group removed. No group matched the given ID", NOT_FOUND)
		return
	}

	writer.WriteHeader(OK)
}

/*
 Encodes a group to a serialized representation. Adds rest links.
*/
func (gs *GroupService) encode(group *model.Group) ([]byte, error) {
	var job = make(map[string]interface{})
	var obj = make(map[string]interface{})
	var links = make([]map[string]string, 6)
	var linkObjs = make([]*model.Link, 6)

	job[MODEL] = obj
	job[LINKS] = links
	obj["id"] = group.Id()
	obj["name"] = group.Name()
	obj["description"] = group.Description()

	var resource = fmt.Sprintf("%s%s%d", GROUP_COLLECTION, "/", group.Id())
	linkObjs[0], _ = model.NewLink(JSON, GROUP_USERS, GET, fmt.Sprintf("%s%s%d", USER_COLLECTION, "?group=", group.Id()))
	linkObjs[1], _ = model.NewLink(JSON, GROUP_OTHERS, GET, fmt.Sprintf("%s%s%d", USER_COLLECTION, "?notgroup=", group.Id()))
	linkObjs[2], _ = model.NewLink(PLAIN, GROUP_ADD_USER, POST, fmt.Sprintf("%s%s", GROUP_COLLECTION, "?user="))
	linkObjs[3], _ = model.NewLink(PLAIN, GROUP_REMOVE_USER, DELETE, fmt.Sprintf("%s%s", GROUP_COLLECTION, "?user="))
	linkObjs[4], _ = model.NewLink(PLAIN, GROUP_UPDATE, POST, resource)
	linkObjs[5], _ = model.NewLink(PLAIN, GROUP_REMOVE, DELETE, resource)

	for cont, link := range linkObjs {
		links[cont] = toMap(link)
	}

	encoded, err := json.Marshal(job)

	return encoded, err
}

/*
 Encodes a collection of groups to a serialized representation. Adds to each one rest links
*/
func (gs *GroupService) encodeSlice(groups []*model.Group) ([]byte, error) {
	var list = make([]string, 1)
	list[0] = "{\n"

	for _, group := range groups {
		var resource = fmt.Sprintf(`"%s%s%d"`, GROUP_COLLECTION, "/", group.Id())
		var encoded, err = gs.encode(group)

		if err != nil {
			return nil, err
		}

		list = append(list, resource, ":", string(encoded), ",\n")
	}

	list[len(list)-1] = "}\n"
	var result = strings.Join(list, "")

	return []byte(result), nil
}

/*
 Decodes a serialized representation of a group into a model object
*/
func (gs *GroupService) decode(message []byte) (*model.Group, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 3)
	var id float64
	var name, description string

	id, ok[0] = job["id"].(float64)
	name, ok[1] = job["name"].(string)
	description, ok[2] = job["description"].(string)

	for _, val := range ok {
		if !val {
			return nil, errors.New(INVALID_PAYLOAD)
		}
	}

	group, err := model.NewGroup(
		uint(id),
		name,
		description,
	)

	return group, err
}
