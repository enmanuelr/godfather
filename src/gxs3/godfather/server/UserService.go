package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"gxs3/godfather/data"
	"gxs3/godfather/model"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

const (
	USER_COLLECTION = "/user" //Privilege service global route

	USER_GROUPS         = "/rels/gf.user-groups"         //Rel for link that retrieves users that belong to a group
	USER_UPDATE         = "/rels/gf.user-update"         //Rel for link that updates a user
	USER_RESET_PASSWORD = "/rels/gf.user-reset-password" //Rel for link that resets a user's password
	USER_REMOVE         = "/rels/gf.user-remove"         //Rel that removes a user

	NAME     = "name"     //Used to extract name values from request queries
	GROUP    = "group"    //Used to extract group values from request queries
	NOTGROUP = "notgroup" //Used to extract "not group" values from request queries
	PASSWORD = "password" //Used to extract password values from request queries
)

/*
 Service implementation that handles access to the user collection.
 When users are retrieved and updated, the password field is always expected to be nil. Only when a new user account
 is being created is there an expectation of populating the password field in the user model. When updating user accounts
 passwords should not be sent, there is a specific end point for resetting passwords.
*/
type UserService struct {
	*BaseService
	dao *data.UserDao
}

/*
 Creates a new user service
*/
func NewUserService(router *mux.Router) *UserService {
	var us = new(UserService)
	us.BaseService = NewBaseService(router)
	var subrouter = router.PathPrefix(USER_COLLECTION).Subrouter()

	var get = func(writer http.ResponseWriter, req *http.Request) { us.get(writer, req) }
	var getByName = func(writer http.ResponseWriter, req *http.Request) { us.getByName(writer, req) }
	var getInGroup = func(writer http.ResponseWriter, req *http.Request) { us.getInGroup(writer, req) }
	var getNotInGroup = func(writer http.ResponseWriter, req *http.Request) { us.getNotInGroup(writer, req) }
	var getAll = func(writer http.ResponseWriter, req *http.Request) { us.getAll(writer, req) }
	var add = func(writer http.ResponseWriter, req *http.Request) { us.add(writer, req) }
	var update = func(writer http.ResponseWriter, req *http.Request) { us.update(writer, req) }
	var resetPassword = func(writer http.ResponseWriter, req *http.Request) { us.resetPassword(writer, req) }
	var remove = func(writer http.ResponseWriter, req *http.Request) { us.remove(writer, req) }

	subrouter.Methods(GET).Path(ID_PATTERN).HandlerFunc(get)
	subrouter.Methods(GET).Path("/{name:[a-zA-Z][a-zA-Z0-9_]*}").HandlerFunc(getByName)
	router.Methods(GET).Path(USER_COLLECTION).Queries(GROUP, "").HandlerFunc(getInGroup)
	router.Methods(GET).Path(USER_COLLECTION).Queries(NOTGROUP, "").HandlerFunc(getNotInGroup)
	router.Methods(GET).MatcherFunc(ExactMatch(USER_COLLECTION)).HandlerFunc(getAll)
	router.Methods(POST).Path(USER_COLLECTION).HandlerFunc(add)
	subrouter.Methods(PUT).Path(ID_PATTERN).HandlerFunc(update)
	subrouter.Methods(POST).Path(ID_PATTERN + "/password").HandlerFunc(resetPassword)
	subrouter.Methods(DELETE).Path(ID_PATTERN).HandlerFunc(remove)

	return us
}

/*
 Initializes the service
*/
func (us *UserService) Init(context map[string]interface{}) {
	var template = context[DB_TEMPLATE].(*data.SqlTemplate)
	var dictionary = context[DB_DICTIONARY].(map[string]string)
	var err error
	us.dao, err = data.NewUserDao(template, dictionary)

	if err != nil {
		panic(err.Error())
	}
}

/*
 Retrieves a user identified by a given ID
 Method: GET - URI: /user/{id}
*/
func (us *UserService) get(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var vars = mux.Vars(req)
	var id, err = strconv.ParseUint(vars[ID], 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	user, err := us.dao.Get(uint(id))

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if user == nil {
		writer.WriteHeader(NOT_FOUND)
		return
	}

	encoded, err := us.encode(user)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.Write(encoded)
}

/*
 Retrieves a user identifed by a user name
 Method: GET - URI: /user/{user-name}
*/
func (us *UserService) getByName(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var name = mux.Vars(req)[NAME]

	var user, err = us.dao.GetByName(name)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if user == nil {
		writer.WriteHeader(NOT_FOUND)
		return
	}

	encoded, err := us.encode(user)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.Write(encoded)
}

/*
 Retrieves all users
 Method: GET - URI: /user
*/
func (us *UserService) getAll(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var users, err = us.dao.GetAll()

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if len(users) < 1 {
		writer.WriteHeader(NOT_FOUND)
		return
	}

	encoded, err := us.encodeSlice(users)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.Write(encoded)
}

/*
 Retrieves all users that are members of a given group
 Method: GET - URI: /user?group={id-group}
*/
func (us *UserService) getInGroup(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var groupStr = req.FormValue(GROUP)
	var idGroup, err = strconv.ParseUint(groupStr, 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	users, err := us.dao.GetInGroup(uint(idGroup))

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if len(users) < 1 {
		writer.WriteHeader(NOT_FOUND)
		return
	}

	encoded, err := us.encodeSlice(users)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.Write(encoded)
}

/*
 Retrieves all users that are *NOT* members of a given group
 Method: GET - URI: /user?notgroup={id-group}
*/
func (us *UserService) getNotInGroup(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var groupStr = req.FormValue(NOTGROUP)
	var idGroup, err = strconv.ParseUint(groupStr, 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	users, err := us.dao.GetNotInGroup(uint(idGroup))

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if len(users) < 1 {
		writer.WriteHeader(NOT_FOUND)
		return
	}

	encoded, err := us.encodeSlice(users)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.Write(encoded)
}

/*
 Adds a new user to the collection. The user is expected in the request payload
 Method: POST - URI: /user
*/
func (us *UserService) add(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var userStr, err = ioutil.ReadAll(req.Body)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	if len(userStr) < 1 {
		http.Error(writer, EMPTY_PAYLOAD, BAD_REQUEST)
		return
	}

	user, err := us.decode(userStr)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	id, err := us.dao.Add(user)

	if err != nil {
		if err == data.CONSTRAINT {
			http.Error(writer, err.Error(), CONFLICT)
		} else {
			http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		}

		return
	}

	user.SetId(id)
	userStr, err = us.encode(user)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.WriteHeader(CREATED)
	writer.Write(userStr)
}

/*
 Updates the details for an existing user. The user is expected in the request payload.
 The password field should be sent nil here. It is ignored by this process. If a user's password needs to be reset
 then the reset password end point should be invoked separately.
 Method: PUT - URI: /user/{id}
*/
func (us *UserService) update(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var vars = mux.Vars(req)
	var id, err = strconv.ParseUint(vars[ID], 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	userStr, err := ioutil.ReadAll(req.Body)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	if len(userStr) < 1 {
		http.Error(writer, EMPTY_PAYLOAD, BAD_REQUEST)
		return
	}

	user, err := us.decode(userStr)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	if uint(id) != user.Id() {
		http.Error(writer, "User ID does not match with ID in URI", BAD_REQUEST)
		return
	}

	records, err := us.dao.Update(user)

	if err != nil {
		if err == data.CONSTRAINT {
			http.Error(writer, err.Error(), CONFLICT)
		} else {
			http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		}

		return
	}

	if records < 1 {
		http.Error(writer, "Can't update user. No user matches the provided ID", NOT_FOUND)
		return
	}

	userStr, err = us.encode(user)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	writer.Header().Set(CONTENT_TYPE, JSON)
	writer.WriteHeader(OK)
	writer.Write(userStr)
}

/*
 Resets a user's password. The new password is expected as a form value, key: "password".
 Method: POST - URI: /user/{id}/password
*/
func (us *UserService) resetPassword(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var vars = mux.Vars(req)
	var id, err = strconv.ParseUint(vars[ID], 10, 32)
	var password = req.FormValue(PASSWORD)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	if len(password) < 1 {
		http.Error(writer, "Password cannot be empty", BAD_REQUEST)
		return
	}

	records, err := us.dao.ResetPassword(uint(id), password)

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if records < 1 {
		http.Error(writer, "Can't reset password. No user matched the given ID", NOT_FOUND)
		return
	}

	writer.WriteHeader(OK)
}

/*
 Removes a user from the collection
 Method: DELETE - URI: /user/{id}
*/
func (us *UserService) remove(writer http.ResponseWriter, req *http.Request) {
	defer Recover(writer)
	var vars = mux.Vars(req)
	var id, err = strconv.ParseUint(vars[ID], 10, 32)

	if err != nil {
		http.Error(writer, err.Error(), BAD_REQUEST)
		return
	}

	records, err := us.dao.Remove(uint(id))

	if err != nil {
		http.Error(writer, err.Error(), INTERNAL_SERVER_ERROR)
		return
	}

	if records < 1 {
		http.Error(writer, "No user removed. No user matched the given ID", NOT_FOUND)
		return
	}

	writer.WriteHeader(OK)
}

/*
 Encodes a user into a serialized representation for transport. Adds REST links.
*/
func (us *UserService) encode(user *model.User) ([]byte, error) {
	var job = make(map[string]interface{})
	var obj = make(map[string]interface{})
	var links = make([]map[string]string, 4)
	var linkObjs = make([]*model.Link, 4)

	job[MODEL] = obj
	job[LINKS] = links
	obj["id"] = user.Id()
	obj["username"] = user.Username()
	obj["password"] = nil
	obj["firstName"] = user.FirstName()
	obj["lastName"] = user.LastName()
	obj["enabled"] = user.Enabled()

	var resource = fmt.Sprintf("%s%s%d", USER_COLLECTION, "/", user.Id())
	linkObjs[0], _ = model.NewLink(JSON, USER_GROUPS, GET, fmt.Sprintf("%s%s%d", GROUP_COLLECTION, "?user=", user.Id()))
	linkObjs[1], _ = model.NewLink(JSON, USER_UPDATE, PUT, resource)
	linkObjs[2], _ = model.NewLink(PLAIN, USER_RESET_PASSWORD, POST, resource+"/password")
	linkObjs[3], _ = model.NewLink(PLAIN, USER_REMOVE, DELETE, resource)

	for cont, link := range linkObjs {
		links[cont] = toMap(link)
	}

	encoded, err := json.Marshal(job)

	return encoded, err
}

/*
 Encodes a collection of users into a serialized representation for transport. Adds REST links to each one.
*/
func (us *UserService) encodeSlice(users []*model.User) ([]byte, error) {
	var list = make([]string, 1)
	list[0] = "{\n"

	for _, user := range users {
		var resource = fmt.Sprintf(`"%s%s%d"`, USER_COLLECTION, "/", user.Id())
		var encoded, err = us.encode(user)

		if err != nil {
			return nil, err
		}

		list = append(list, resource, ":", string(encoded), ",\n")
	}

	list[len(list)-1] = "\n}\n"
	var result = strings.Join(list, "")

	return []byte(result), nil
}

/*
 Decodes a user from a serialized representation into a model object.
*/
func (us *UserService) decode(message []byte) (*model.User, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var passwordStr *string
	var ok = make([]bool, 6)
	var id float64
	var username, firstName, lastName, str string
	var enabled bool

	id, ok[0] = job["id"].(float64)
	username, ok[1] = job["username"].(string)
	firstName, ok[2] = job["firstName"].(string)
	lastName, ok[3] = job["lastName"].(string)
	enabled, ok[4] = job["enabled"].(bool)

	if job["password"] != nil {
		str, ok[5] = job["password"].(string)
		passwordStr = &str
	}

	for _, val := range ok {
		if !val {
			return nil, errors.New(INVALID_PAYLOAD)
		}
	}

	user, err := model.NewUser(
		uint(id),
		username,
		passwordStr,
		firstName,
		lastName,
		enabled,
	)

	return user, err
}
