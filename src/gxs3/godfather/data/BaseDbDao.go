/*
 Data base abstraction layer. Serves as a platform specific ORM to swap data in and out of the data store.
 The current implementation is direclty tied to sqlite. However, I would like to improve it in the future where
 I can plug in library / data store / platform specific stuff, and leave this layer as something generic, so I can
 use it interchangably with different data stores. I will need more data stores in the future, and forking is not
 a good approach in this case.
*/
package data

import (
	"bytes"
	"encoding/json"
	"errors"
	gsl "github.com/gwenn/gosqlite"
	"gxs3/godfather/model"
	"io/ioutil"
)

var (
	/*
	 Error that is raised when a constraint violation occurs during the execution of a query.
	 This error is dfferent from the rest. This is expected to happen during the executin of transactions.
	 The underlying native error is abstracted, and this is returned to upper levels.
	*/
	CONSTRAINT error = errors.New("Constraint Violation")
)

/*
 Basic implementation of a database driven data access object. Provides a base structure that DAO implementations
 use. It is expected that DAO implementations will embed this type in their declarations.
*/
type BaseDbDao struct {
	template   *SqlTemplate      // SQL Template to use in the DAO
	dictionary map[string]string // Dictionary containing queries
}

/*
 Creates a new Base Database Driven Data Access object. Rarely will this object be used alone. A more likely
 scenario is to use this as part of creating a DAO implementation which embeds this type.
*/
func NewBaseDbDao(template *SqlTemplate, dictionary map[string]string) (*BaseDbDao, error) {
	if template == nil || dictionary == nil {
		return nil, errors.New(model.NIL_ARGUMENT)
	}

	var dbDao = BaseDbDao{template, dictionary}
	return &dbDao, nil
}

/*
 Reads a query file, and generates a map with all quries found. The query file is expected to be json formatted.
 The structure is simple: a single level key-value dictionary. Keys are unique identifiers while values are queries.
 Query stirngs can be divided in multiple lines. Even though that's not valid json, this function will remove all new
 lines before attempting to unmarshal to provide this flexibility, which makes the queries easier to read.
*/
func ReadQueryFile(location string) (map[string]string, error) {
	var contents, err = ioutil.ReadFile(location)

	if err != nil {
		return nil, err
	}

	contents = bytes.Replace(contents, []byte("\n"), []byte{}, -1)
	contents = bytes.Replace(contents, []byte("\t"), []byte{}, -1)

	if err != nil {
		return nil, err
	}

	var parsed interface{}
	err = json.Unmarshal(contents, &parsed)

	if err != nil {
		return nil, err
	}

	var job = parsed.(map[string]interface{})
	var result = make(map[string]string)

	for key, value := range job {
		result[key] = value.(string)
	}

	return result, nil
}

//Returns the template which is currently being used by this DAO
func (dbdao *BaseDbDao) Template() *SqlTemplate {
	return dbdao.template
}

/*
 When the DAO was initialized a query file was loaded. This function will search the loaded queries for one
 that matches the provided key. 
*/
func (dbdao *BaseDbDao) GetQuery(key string) string {
	return dbdao.dictionary[key]
}

/*
 Utility function which takes a bool value and convertis to to 0 (false) or 1 (true). Useful
 when decoding data coming from database. Some data stores don't natively support boolean values, and use 0/1
 values instead.
*/
func ToNumber(boolean bool) uint {
	var result uint

	if boolean {
		result = 1
	} else {
		result = 0
	}

	return result
}

/*
 Checks the error passed as an argument. If the error was generated due to a constraint violation, then this function
 returns the CONSTRAINT value declared in this package. This is used to abstract higher levels from library/driver
 specific logic. If the value is any other type of error or nil, it is returned as is.
*/
func CheckConstraintError(err error) error {
	if err != nil {
		serr, ok := err.(*gsl.StmtError)

		if ok && serr.Code() == gsl.ErrConstraint {
			err = CONSTRAINT
		}
	}

	return err
}
