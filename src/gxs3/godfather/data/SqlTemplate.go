package data

import (
	"database/sql"
	"errors"
	"gxs3/godfather/model"
	"time"
)

const (
	/*
	 Maximum number of times the template will attempt to retrieve a connection from the pool before 
	 giving up.
	*/
	MAX_RETRIES = 10

	/*
	 Amount of time the template will wait in between attempts to acquire a connection from the pool.
	 Expressed in nanoseconds.
	*/
	RETRY_SLEEP = 1e8 //0.1 seconds in nanoseconds
)

/*
 The SQL Template is a facility to make invoking the database a much easier task. The SQL Template takes care of
 all the logistics required to make the actual invocation. Clients simply need to provide the queries that need
 to be executed, any possible parameters, and if a return value is expected, a mapper function that will help translate
 the results
*/
type SqlTemplate struct {
	pool *ConnectionPool //Connection pool. Will be used to acquire connections to execute database queries
}

/*
 Defines a function signature for mappers. Mappers are used to translate database rows into model objects. When invoking
 Select and SelectFirst a mapper must be provided. This mapper have the necessary logic to translate into models.
*/
type Mapper func(*sql.Rows) (interface{}, error)

/*
 Creates a new sql template. the connection pool to be used must be provided
*/
func NewSqlTemplate(pool *ConnectionPool) (*SqlTemplate, error) {
	if pool == nil {
		return nil, errors.New(model.NIL_ARGUMENT)
	}

	var template = SqlTemplate{pool}
	return &template, nil
}

/*
 Performs a select query. This execution will return zero or more results. All results will be given back in a slice.
 The provided mapper will be used to map the rows into models. If the query has any prepared state place holders, then 
 the corresponding values are expected in the args parameter
*/
func (template *SqlTemplate) Select(query string, mapper Mapper, args ...interface{}) ([]interface{}, error) {
	var client, err = getClient(template.pool)

	if err != nil {
		return nil, err
	}

	defer template.pool.ReturnClient(client)
	stmt, err := client.Prepare(query)

	if err != nil {
		return nil, err
	}

	defer stmt.Close()
	rows, err := stmt.Query(args...)

	if err != nil {
		return nil, err
	}

	defer rows.Close()
	var result = make([]interface{}, 0)

	for rows.Next() {
		model, err := mapper(rows)

		if err != nil {
			return nil, err
		}

		result = append(result, model)
	}

	return result, nil
}

/*
 Performs a select query. Only the first row will be taken, if such is yielded, if not, nil will be returned.
 The provided mapper will be used to map the rows into models. If the query has any prepared state place holders, then 
 the corresponding values are expected in the args parameter
*/
func (template *SqlTemplate) SelectFirst(query string, mapper Mapper, args ...interface{}) (interface{}, error) {
	var client, err = getClient(template.pool)

	if err != nil {
		return nil, err
	}

	defer template.pool.ReturnClient(client)
	stmt, err := client.Prepare(query)

	if err != nil {
		return nil, err
	}

	defer stmt.Close()
	rows, err := stmt.Query(args...)

	if err != nil {
		return nil, err
	}

	defer rows.Close()
	var found = rows.Next()
	var result interface{} = nil

	if found {
		result, err = mapper(rows)

		if err != nil {
			return nil, err
		}
	}

	return result, nil
}

/*
 Performs an insert query. If the insertion was made to a table with auto increment, the last generated ID will be returned.
 This is veyr useful in order ot give IDs to newly created models.
*/
func (template *SqlTemplate) Insert(query string, args ...interface{}) (int64, error) {
	var id int64
	var client, err = getClient(template.pool)

	if err != nil {
		return id, err
	}

	defer template.pool.ReturnClient(client)
	stmt, err := client.Prepare(query)

	if err != nil {
		return id, err
	}

	result, err := stmt.Exec(args...)

	if err != nil {
		return id, err
	}

	id, err = result.LastInsertId()
	err = stmt.Close()

	if err != nil {
		return id, err
	}

	return id, nil
}

/*
 Performs update and delete queries. Returns the number of rows affected by the execution of the query. If the result is
 0, then the invocation had no effect. 
*/
func (template *SqlTemplate) Update(query string, args ...interface{}) (int64, error) {
	var records int64
	var client, err = getClient(template.pool)

	if err != nil {
		return records, err
	}

	defer template.pool.ReturnClient(client)
	stmt, err := client.Prepare(query)

	if err != nil {
		return records, err
	}

	result, err := stmt.Exec(args...)

	if err != nil {
		return records, err
	}

	records, err = result.RowsAffected()

	if err != nil {
		return 0, err
	}

	err = stmt.Close()

	if err != nil {
		return 0, err
	}

	return records, err
}

/*
 Attempts ot retreive a client from the pool. If the pool has no available connections, it will retry up to MAX_RETRIES
 times. Between each one of those time, it will wait RETRY_SLEEP.
*/
func getClient(pool *ConnectionPool) (client *sql.DB, err error) {
	if pool == nil {
		return nil, errors.New(model.NIL_ARGUMENT)
	}

	for cont := 0; cont < MAX_RETRIES; cont++ {
		client, err = pool.GetClient()

		if err == nil {
			break
		} else if err.Error() == NO_FREE_CONNECTION {
			time.Sleep(RETRY_SLEEP)
		} else if err != nil {
			return nil, err
		}
	}

	return client, nil
}
