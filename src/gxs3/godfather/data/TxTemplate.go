package data

import (
	"database/sql"
	"errors"
	"gxs3/godfather/model"
)

/*
 A specialized version of the SQL template that is used to run database transactions. Works in a very basic way.
 Attempts to execute all SQL queries in a transaction function. If it fails at any point, a rollback is executed.
 Otherwise a commit is executed at the end of the successful execution.
*/
type TxTemplate struct {
	pool *ConnectionPool //Connection pool used to get connections to run SQL commands
}

/*
 A funcion signature which is used to identify transaction functions. A transaction function will contain one or
 more (ideally two or more) sql commands. The transaction template will be passed to it. ALL SQL COMMANDS MUST BE
 RUN USING THE PASSED TRANSACTION TEMPLATE. This is what guarantees that all commands are run within the same 
 connection and transaction
*/
type Transaction func(template *TxTemplate, tx *sql.Tx) error

/*
 Creates a new transaction tamplate. Requires and existing SQL template to be passed as an argument. It is used
 to extract the connection pool from it, and access connections to execute SQL commands
*/
func NewTxTemplate(st *SqlTemplate) (*TxTemplate, error) {
	if st == nil {
		return nil, errors.New(model.NIL_ARGUMENT)
	}

	var template = TxTemplate{st.pool}
	return &template, nil
}

/*
 Executes a transaction. If an error is encountered at any point, the transaction is rolled back. If the transaction
 finishes successfully, then a commit is performed.
*/
func (template *TxTemplate) Execute(transaction Transaction) error {
	var client, err = getClient(template.pool)

	if err != nil {
		return err
	}

	defer template.pool.ReturnClient(client)
	tx, err := client.Begin()

	if err != nil {
		tx.Rollback()
		return err
	}

	err = transaction(template, tx)

	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return nil
}

/*
 Performs a select query within the context of a transaction. Receives the tx that is currently being used.
 The query will result in zero or more results. A slice will be returned with results. If none are found an empty
 slice is produced. The mapper is used to translate from rows to model objects.
*/
func (template *TxTemplate) Select(tx *sql.Tx, query string, mapper Mapper, args ...interface{}) ([]interface{}, error) {
	var stmt, error = tx.Prepare(query)

	if error != nil {
		return nil, error
	}

	defer stmt.Close()
	rows, error := stmt.Query(args...)

	if error != nil {
		return nil, error
	}

	defer rows.Close()
	var result = make([]interface{}, 1)

	for rows.Next() {
		model, error := mapper(rows)

		if error != nil {
			return nil, error
		}

		result = append(result, model)
	}

	return result, nil
}

/*
 Performs a select query within the context of a transaction, which is only expected to return one element. Receives the
 tx that is currently being used. Will only attempt to receive the first row, if such exists, otherwise nil is returned.
 The mapper is used to translate from rows to model objects.
*/
func (template *TxTemplate) SelectFirst(tx *sql.Tx, query string, mapper Mapper, args ...interface{}) (interface{}, error) {
	var stmt, error = tx.Prepare(query)

	if error != nil {
		return nil, error
	}

	defer stmt.Close()
	rows, error := stmt.Query(args...)

	if error != nil {
		return nil, error
	}

	defer rows.Close()
	rows.Next()
	result, error := mapper(rows)

	if error != nil {
		return nil, error
	}

	return result, nil
}

/*
 Performs an Insert query within the context of a transaction. Receives the tx of the current transaction. If the insert
 is performed on a table with auto increment, the function will returned the last insert ID.
*/
func (template *TxTemplate) Insert(tx *sql.Tx, query string, args ...interface{}) (int64, error) {
	var id int64
	var stmt, error = tx.Prepare(query)

	if error != nil {
		return id, error
	}

	defer stmt.Close()
	result, error := stmt.Exec(args...)

	if error != nil {
		return id, error
	}

	id, error = result.LastInsertId()

	if error != nil {
		return id, error
	}

	return id, nil
}

/*
 Performs an Update/Delete query within the conext of a transaction. Receives the text of the current transaciton. 
 Returns the number of rows affected by the query. If zero is returned, the invocation had no effect.
*/
func (template *TxTemplate) Update(tx *sql.Tx, query string, args ...interface{}) (int64, error) {
	var records int64
	var stmt, error = tx.Prepare(query)

	if error != nil {
		return records, error
	}

	defer stmt.Close()
	result, error := stmt.Exec(args...)

	if error != nil {
		return records, error
	}

	records, error = result.RowsAffected()

	if error != nil {
		return records, error
	}

	return records, nil
}
