package data

import (
	"code.google.com/p/go.crypto/bcrypt"
	"database/sql"
	"errors"
	"gxs3/godfather/model"
)

/*
 Data driven data access object implementation for the User model
*/
type UserDao struct {
	*BaseDbDao
}

/*
 Creates a new User data access object. Receives the sql template to be used, as well as the query dictionary
*/
func NewUserDao(template *SqlTemplate, dictionary map[string]string) (*UserDao, error) {
	var base, err = NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	var ud = &UserDao{base}
	return ud, nil
}

/*
 Maps a database row to a user model object
*/
func mapUser(rows *sql.Rows) (interface{}, error) {
	var id, enabled uint
	var username, firstName, lastName string
	var password *string

	var err = rows.Scan(&id, &username, &password, &firstName, &lastName, &enabled)

	if err != nil {
		return nil, err
	}

	user, err := model.NewUser(id, username, password, firstName, lastName, enabled == 1)
	return user, err
}

/*
 Retrieves the user identified by the given ID
*/
func (ud *UserDao) Get(id uint) (*model.User, error) {
	var query = ud.GetQuery("gf.get-user")
	var result, err = ud.Template().SelectFirst(query, mapUser, id)

	if result == nil || err != nil {
		return nil, err
	}

	var user = result.(*model.User)
	return user, nil
}

/*
 Retrieves the user identified by the given user name
*/
func (ud *UserDao) GetByName(username string) (*model.User, error) {
	if len(username) < 1 {
		return nil, errors.New(model.EMPTY_STRING)
	}

	var query = ud.GetQuery("gf.get-user-by-username")
	var result, err = ud.Template().SelectFirst(query, mapUser, username)

	if result == nil || err != nil {
		return nil, err
	}

	var user = result.(*model.User)
	return user, nil
}

/*
 Retrieves all users
*/
func (ud *UserDao) GetAll() ([]*model.User, error) {
	var query = ud.GetQuery("gf.get-all-users")
	var result, err = ud.Template().Select(query, mapUser)

	return ud.cast(result), err
}

/*
 Retrieves all users which are members of the given group
*/
func (ud *UserDao) GetInGroup(idGroup uint) ([]*model.User, error) {
	var query = ud.GetQuery("gf.get-users-in-group")
	var result, err = ud.Template().Select(query, mapUser, idGroup)

	return ud.cast(result), err
}

/*
 Retrieves all users which are NOT members of the given group
*/
func (ud *UserDao) GetNotInGroup(idGroup uint) ([]*model.User, error) {
	var query = ud.GetQuery("gf.get-users-not-in-group")
	var result, err = ud.Template().Select(query, mapUser, idGroup)

	return ud.cast(result), err
}

/*
 Add a new user ot the collection
*/
func (ud *UserDao) Add(user *model.User) (uint, error) {
	var query = ud.GetQuery("gf.add-user")
	var password []byte

	if user.Password() != nil {
		password, _ = bcrypt.GenerateFromPassword([]byte(*user.Password()), bcrypt.MaxCost)
	}

	var result, err = ud.Template().Insert(query,
		user.Username(),
		string(password),
		user.FirstName(),
		user.LastName(),
		ToNumber(user.Enabled()),
	)

	err = CheckConstraintError(err)
	return uint(result), err
}

/*
 Update an existing user. The user's ID is used as a reference to match the row to update.
 The password property is NOT updated by this action. It is explicitly left out.
*/
func (ud *UserDao) Update(user *model.User) (uint, error) {
	var query = ud.GetQuery("gf.update-user")
	var result, err = ud.Template().Update(query, user.Username(),
		user.FirstName(),
		user.LastName(),
		ToNumber(user.Enabled()),
		user.Id())

	err = CheckConstraintError(err)
	return uint(result), err
}

/*
 Updates a user's password. No other fields are updated in tihs action. 
*/
func (ud *UserDao) ResetPassword(idUser uint, password string) (int64, error) {
	var query = ud.GetQuery("gf.reset-user-password")
	encrypted, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MaxCost)

	var result, err = ud.Template().Update(query, encrypted, idUser)
	return result, err
}

/*
 Removes a user from the collection
*/
func (ud *UserDao) Remove(idUser uint) (uint, error) {
	var txResult uint
	var txt, err = NewTxTemplate(ud.Template())

	if err != nil {
		return 0, err
	}

	var transaction = func(txt *TxTemplate, tx *sql.Tx) error {
		var query = ud.GetQuery("gf.remove-user-from-all-groups")
		var result, err = txt.Update(tx, query, idUser)

		if err != nil {
			tx.Rollback()
			return err
		}

		query = ud.GetQuery("gf.remove-user")
		result, err = txt.Update(tx, query, idUser)

		if err != nil {
			tx.Rollback()
			return err
		}

		txResult = uint(result)
		return nil
	}

	err = txt.Execute(transaction)
	return txResult, err
}

/*
 Casts a slice of user objects with interface{} types into user model types
*/
func (ud *UserDao) cast(result []interface{}) []*model.User {
	if result == nil {
		return nil
	}

	var users = make([]*model.User, len(result))

	for cont, r := range result {
		users[cont] = r.(*model.User)
	}

	return users
}
