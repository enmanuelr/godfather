package data

import (
	"database/sql"
	"gxs3/godfather/model"
)

/*
 Database driven data access object implementation for the Group model
*/
type GroupDao struct {
	*BaseDbDao
}

/*
 Creates a new Group DAO. Requires a sql template and the working query dictionary
*/
func NewGroupDao(template *SqlTemplate, dictionary map[string]string) (*GroupDao, error) {
	var base, err = NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	var gd = &GroupDao{base}
	return gd, nil
}

/*
 Maps a row from the database to a group object.
*/
func mapGroup(rows *sql.Rows) (interface{}, error) {
	var id uint
	var name, description string

	var err = rows.Scan(&id, &name, &description)

	if err != nil {
		return nil, err
	}

	group, err := model.NewGroup(id, name, description)
	return group, err
}

/*
 Retrieves the group identified by the provided ID. 
*/
func (gd *GroupDao) Get(id uint) (*model.Group, error) {
	var query = gd.GetQuery("gf.get-group")
	var result, err = gd.Template().SelectFirst(query, mapGroup, id)

	if result == nil || err != nil {
		return nil, err
	}

	var group = result.(*model.Group)
	return group, nil
}

/*
 Retrieves all groups which a user is a member of
*/
func (gd *GroupDao) GetByUser(idUser uint) ([]*model.Group, error) {
	var query = gd.GetQuery("gf.get-user-groups")
	var result, err = gd.Template().Select(query, mapGroup, idUser)

	return gd.cast(result), err
}

/*
 Retrieves all existing groups
*/
func (gd *GroupDao) GetAll() ([]*model.Group, error) {
	var query = gd.GetQuery("gf.get-all-groups")
	var result, err = gd.Template().Select(query, mapGroup)

	return gd.cast(result), err
}

/*
 Adds a group to the collection. The ID of the new group will be returned.
*/
func (gd *GroupDao) Add(group *model.Group) (uint, error) {
	var query = gd.GetQuery("gf.add-group")
	var result, err = gd.Template().Insert(query,
		group.Name(),
		group.Description(),
	)

	err = CheckConstraintError(err)
	return uint(result), err
}

/*
 Updates the details of a group. Uses the ID to match the existing row which will be updated.
*/
func (gd *GroupDao) Update(group *model.Group) (uint, error) {
	var query = gd.GetQuery("gf.update-group")
	var result, err = gd.Template().Update(query,
		group.Name(),
		group.Description(),
		group.Id(),
	)

	err = CheckConstraintError(err)
	return uint(result), err
}

/*
 Adds a user to a group. The user will then be a member, and will have the benefits of belonging to the group.
 If the user already belongs to the group an error is raised
*/
func (gd *GroupDao) AddUser(idGroup, idUser uint) (uint, error) {
	var query = gd.GetQuery("gf.add-user-to-group")
	var result, err = gd.Template().Update(query,
		idUser,
		idGroup,
	)

	err = CheckConstraintError(err)
	return uint(result), err
}

/*
 Removes a user from a group. If the user never belonged to the group, this invocation raises no error 
 but has no effect
*/
func (gd *GroupDao) RemoveUser(idGroup, idUser uint) (uint, error) {
	var query = gd.GetQuery("gf.remove-user-from-group")
	var result, err = gd.Template().Update(query,
		idUser,
		idGroup,
	)

	return uint(result), err
}

/*
 Removes a group from the collection
*/
func (gd *GroupDao) Remove(idGroup uint) (uint, error) {
	var txResult uint
	var txt, err = NewTxTemplate(gd.Template())

	if err != nil {
		return 0, err
	}

	var transaction = func(txt *TxTemplate, tx *sql.Tx) error {
		var query = gd.GetQuery("gf.remove-all-users-from-group")
		var result, err = txt.Update(tx, query, idGroup)

		if err != nil {
			tx.Rollback()
			return err
		}

		query = gd.GetQuery("gf.revoke-all")
		result, err = txt.Update(tx, query, idGroup)

		if err != nil {
			tx.Rollback()
			return err
		}

		query = gd.GetQuery("gf.remove-group")
		result, err = txt.Update(tx, query, idGroup)

		if err != nil {
			tx.Rollback()
			return err
		}

		txResult = uint(result)
		return nil
	}

	err = txt.Execute(transaction)
	return txResult, err
}

/*
 Casts a slice of group objects as interface{} types into group types
*/
func (gd *GroupDao) cast(result []interface{}) []*model.Group {
	if result == nil {
		return nil
	}

	var groups = make([]*model.Group, len(result))

	for cont, r := range result {
		groups[cont] = r.(*model.Group)
	}

	return groups
}
