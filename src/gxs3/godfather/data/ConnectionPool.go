package data

import (
	"database/sql"
	"errors"
	"sync"
)

const (
	//Error raised when no connections are available in the pool
	NO_FREE_CONNECTION = "No connections are avilable in the pool"

	//Error raised ehen the connection being returned to the pool, is not recognized as belonging
	UNRECOGNIZED_CONNECTION = "Provided connection was not recognized as coming from this pool"
)

/*
 Very basic connection pool. Generates a pool with a number of resources on startup. Has simple mechanisims
 for retrieving, returning rsources from the pool and shutting down the pool and all of its resources.
 Uses mutexes to make sure resources are not assigned to more than one thread
*/
type ConnectionPool struct {
	dataSource string     //string that describes how to connect to the database. Will be used to spawn new connections
	free       []*sql.DB  //Slice with all available free connections
	busy       []*sql.DB  // Slice with all avaialble busy connections
	mutex      sync.Mutex //Mutex used to lock the ppol when resources are being retrieved and returned
}

/* 
 Creates a new connection pool. Invoker must specify the driver to use. This will create connections with the
 built-in database library. The datasource provides connection specific details, and lastly the size of the pool
 will be provided as well.
*/
func NewConnectionPool(driver string, dataSource string, size uint) (*ConnectionPool, error) {
	var pool = new(ConnectionPool)
	pool.dataSource = dataSource
	pool.free = make([]*sql.DB, size)
	pool.busy = make([]*sql.DB, 0)

	for cont := uint(0); cont < size; cont++ {
		client, err := sql.Open(driver, dataSource)

		if err != nil {
			return nil, err
		}

		//enable foreign key support in sqlite. This is porobably gonna cause problems
		//when I use this library in other datastores. I need to figure out how to abstract
		//this logic and keep this class pure
		if driver == "sqlite3" {
			client.Exec("PRAGMA foreign_keys = ON")
		}

		pool.free[cont] = client
	}

	return pool, nil
}

/*
 Acquires a connection from the pool. If there are no available connections an error is returned.
*/
func (pool *ConnectionPool) GetClient() (*sql.DB, error) {
	pool.mutex.Lock()
	defer pool.mutex.Unlock()

	if len(pool.free) < 1 {
		return nil, errors.New(NO_FREE_CONNECTION)
	}

	var client = pool.free[0]
	pool.free = pool.free[1:]
	pool.busy = append(pool.busy, client)

	return client, nil
}

/*
 Returns a connection back to the pool. If the connection being retruned is not recognized as one of the members
 of the pool, it will not be integrate, and an error will be returned
*/
func (pool *ConnectionPool) ReturnClient(client *sql.DB) error {
	pool.mutex.Lock()
	defer pool.mutex.Unlock()

	for cont, size := 0, len(pool.busy); cont < size; cont++ {
		if client == pool.busy[cont] {
			pool.busy = append(pool.busy[:cont], pool.busy[cont+1:]...)
			pool.free = append(pool.free, client)
			return nil
		}
	}

	return errors.New(UNRECOGNIZED_CONNECTION)
}

/*
 Shuts down the pool, and all of its connections. All connections will be shut down, irrelevant of wether they are
 in use or not
*/
func (pool *ConnectionPool) Close() {
	pool.mutex.Lock()
	defer pool.mutex.Unlock()

	for _, client := range pool.free {
		client.Close()
	}

	for _, client := range pool.busy {
		client.Close()
	}
}
