package data

import (
	"database/sql"
	"gxs3/godfather/model"
)

/*
 Database driven data access object implementation for the privilege model.
 This DAO is almost exclusively read-only. Privileges are created when the system is installed. Since they are
 very closly tied to application logic, it's difficult to amange them dynamically. The only write operations supported
 are granting and revoking privileges from user groups.
*/
type PrivilegeDao struct {
	*BaseDbDao
}

/*
 Creates a new privilege data access object. Requires the sql template, and query dictionary
*/
func NewPrivilegeDao(template *SqlTemplate, dictionary map[string]string) (*PrivilegeDao, error) {
	var base, err = NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	var pd = &PrivilegeDao{base}
	return pd, nil
}

/*
 Maps a row from the database with privilege data into a model object
*/
func mapPrivilege(rows *sql.Rows) (interface{}, error) {
	var id uint
	var module, key, name, description string

	var err = rows.Scan(&id, &module, &key, &name, &description)

	if err != nil {
		return nil, err
	}

	priv, err := model.NewPrivilege(id, module, key, name, description)
	return priv, err
}

/*
 Retrieves the privilege identified by the given ID
*/
func (pd *PrivilegeDao) Get(id uint) (*model.Privilege, error) {
	var query = pd.GetQuery("gf.get-priv")
	var result, err = pd.Template().SelectFirst(query, mapPrivilege, id)

	if result == nil || err != nil {
		return nil, err
	}

	var priv = result.(*model.Privilege)
	return priv, nil
}

/*
 Retrieves all existing prvileges
*/
func (pd *PrivilegeDao) GetAll() ([]*model.Privilege, error) {
	var query = pd.GetQuery("gf.get-privs")
	var result, err = pd.Template().Select(query, mapPrivilege)

	return pd.cast(result), err
}

/*
 Retrieves all privileges that belong to the given module
*/
func (pd *PrivilegeDao) GetByModule(module string) ([]*model.Privilege, error) {
	var query = pd.GetQuery("gf.get-privs-by-module")
	var result, err = pd.Template().Select(query, mapPrivilege, module)

	return pd.cast(result), err
}

/*
 Retrieves all privileges that have been granted to a group
*/
func (pd *PrivilegeDao) GetGroupGranted(idGroup uint) ([]*model.Privilege, error) {
	var query = pd.GetQuery("gf.get-group-granted-privs")
	var result, err = pd.Template().Select(query, mapPrivilege, idGroup)

	return pd.cast(result), err
}

/*
 Retrieves all privileges which have NOT been granted to a group
*/
func (pd *PrivilegeDao) GetGroupNotGranted(idGroup uint) ([]*model.Privilege, error) {
	var query = pd.GetQuery("gf.get-group-not-granted-privs")
	var result, err = pd.Template().Select(query, mapPrivilege, idGroup)

	return pd.cast(result), err
}

/*
 Get all privileges that have been granted to a user. Privileges are not granted to a user directly.
 This is really all privileges which have been granted to all the groups a user is subscribed to.
*/
func (pd *PrivilegeDao) GetUserGranted(idUser uint) ([]*model.Privilege, error) {
	var query = pd.GetQuery("gf.get-user-granted-privs")
	var result, err = pd.Template().Select(query, mapPrivilege, idUser)

	return pd.cast(result), err
}

/*
 Grant a privilege to a group
*/
func (pd *PrivilegeDao) Grant(idPriv, idGroup uint) (uint, error) {
	var query = pd.GetQuery("gf.grant-priv")
	var records, err = pd.Template().Update(query, idPriv, idGroup)

	err = CheckConstraintError(err)
	return uint(records), nil
}

/*
 Revoke (take away) a privilege from a group
*/
func (pd *PrivilegeDao) Revoke(idPriv, idGroup uint) (uint, error) {
	var query = pd.GetQuery("gf.revoke-priv")
	var records, err = pd.Template().Update(query, idPriv, idGroup)

	if err != nil {
		return 0, err
	}

	return uint(records), nil
}

/*
 Casts a slice of privilege objects from interface{} type to privilege models
*/
func (pd *PrivilegeDao) cast(result []interface{}) []*model.Privilege {
	if result == nil {
		return nil
	}

	var privs = make([]*model.Privilege, len(result))

	for cont, r := range result {
		privs[cont] = r.(*model.Privilege)
	}

	return privs
}
