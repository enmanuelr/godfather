package model

import (
	"errors"
	"fmt"
)

/*
 Represents a user account in the system. Users have the ability to log in, and depending on the privileges assigned to
 the groups he/she belongs to will be able to perform tasks within the application
*/
type User struct {
	*AbstractModel
	username  string
	password  *string
	firstName string
	lastName  string
	enabled   bool
}

/*
 Creates a new user. The password field may be passed as nil, and depending on the context that is usually what should be
 done. The only time the password field is expected to be populated is when a new user account is being created. In all
 other circumstances it will be ignored, and it is encouraged to not be populated. To update an existing user's password
 there are alternative mechanisms made available in the data package.
*/
func NewUser(id uint, username string, password *string, firstName string, lastName string, enabled bool) (*User, error) {
	if len(username) < 1 || len(firstName) < 1 {
		return nil, errors.New(EMPTY_STRING)
	}

	var u = User{NewModel(id), username, password, firstName, lastName, enabled}

	return &u, nil
}

/*
 Returns a string representation of the user
*/
func (u *User) String() string {
	return fmt.Sprintf("%s %s", u.firstName, u.lastName)
}

func (u *User) Username() string {
	return u.username
}

func (u *User) SetUsername(username string) error {
	if len(username) < 1 {
		return errors.New(EMPTY_STRING)
	}

	u.username = username
	return nil
}

/*
 The password field should be nil most of the time. It is only expected to hold a value when a new user account is being
 created.
*/
func (u *User) Password() *string {
	return u.password
}

func (u *User) SetPassword(password *string) error {
	if len(*password) < 1 {
		return errors.New(EMPTY_STRING)
	}

	u.password = password
	return nil
}

func (u *User) FirstName() string {
	return u.firstName
}

func (u *User) SetFirstName(firstName string) error {
	if len(firstName) < 1 {
		return errors.New(EMPTY_STRING)
	}

	u.firstName = firstName
	return nil
}

func (u *User) LastName() string {
	return u.lastName
}

func (u *User) SetLastName(lastName string) error {
	if len(lastName) < 1 {
		return errors.New(EMPTY_STRING)
	}

	u.lastName = lastName
	return nil
}

func (u *User) Enabled() bool {
	return u.enabled
}

func (u *User) SetEnabled(enabled bool) {
	u.enabled = enabled
}
