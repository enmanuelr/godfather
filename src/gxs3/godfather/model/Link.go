package model

import "errors"

/*
 Represents a hypermedia link. Links are used so that server programs can dictate next possible states in an application
 flow instead of leaving the responsability to the client. Google REST/HATEOAS for more info. While there is no perfect
 standard for these links, this is how they will be used in the context of this app:

 Content Type : Will dictate the type of content that will be received when making this request.
 Rel          : Relation identifier key. Should be unique in the app.
 Method       : HTTP method which should be invoked to perform this transaction
 URI          : URI which should be invoked.

 When retrieving elements from the server, each element will come with it's set of links. The client should always follow
 the provided links to know which possible next steps are available. This will avoid double logic on client/server, easier
 upgrades to state logic, and avoid URL construction on the client side.

 There are some minor exceptions to this approach. Some URIs will be used for search criteria, looking up ranges, etc.
 In these cases, it is not easy to know before hand what the URI will be. In these cases, a partial URI is returned, and
 the client must complete it to perform the transaction. These are exceptions, in the rest of cases, URIs should be used
 as returned from the server.
*/
type Link struct {
	contentType string
	rel         string
	method      string
	uri         string
}

/*
 Creates a new Link
*/
func NewLink(contentType, rel, method, uri string) (*Link, error) {
	if len(contentType) < 1 || len(rel) < 1 || len(method) < 1 || len(uri) < 1 {
		return nil, errors.New(EMPTY_STRING)
	}

	var link = &Link{contentType, rel, method, uri}
	return link, nil
}

func (l *Link) ContentType() string {
	return l.contentType
}

func (l *Link) SetContentType(contentType string) error {
	if len(contentType) < 1 {
		return errors.New(EMPTY_STRING)
	}

	l.contentType = contentType
	return nil
}

func (l *Link) Rel() string {
	return l.rel
}

func (l *Link) SetRel(rel string) error {
	if len(rel) < 1 {
		return errors.New(EMPTY_STRING)
	}

	l.rel = rel
	return nil
}

func (l *Link) Method() string {
	return l.method
}

func (l *Link) SetMethod(method string) error {
	if len(method) < 1 {
		return errors.New(EMPTY_STRING)
	}

	l.method = method
	return nil
}

func (l *Link) Uri() string {
	return l.uri
}

func (l *Link) SetUri(uri string) error {
	if len(uri) < 1 {
		return errors.New(EMPTY_STRING)
	}

	l.uri = uri
	return nil
}
