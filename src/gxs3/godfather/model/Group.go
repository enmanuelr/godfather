/*
 Collection of models. A model is a representation of an object or concept which is being represented in the system.
 These models are the basic elements of communication in between layers in the system, implementations and communications
 to other apps that may connect.

 Despite the fact that a lot of go code uses publicly accessible struct properties, I am shying away from that practice
 for several reasons. All models will implement getters and setters. Unless they have some complex logic, other than
 retrieving values and setting/error checking respectively, I will not comment then, since it's pretty obvious what they do.
*/
package model

import "errors"

/*
 Represents a user group. User's can be subscribed to one or more groups. The user will inherit all privileges which
 have been granted to all the groups he/she is subscribed to.
*/
type Group struct {
	*AbstractModel
	name        string //Group name
	description string //Group description
}

/*
 Creates a new group.
*/
func NewGroup(id uint, name string, description string) (*Group, error) {
	if len(name) < 1 || len(description) < 1 {
		return nil, errors.New(EMPTY_STRING)
	}

	var g = Group{NewModel(id), name, description}
	return &g, nil
}

/*
 Returns a string representation of a group
*/
func (g *Group) String() string {
	return g.name
}

func (g *Group) Name() string {
	return g.name
}

func (g *Group) SetName(name string) error {
	if len(name) < 1 {
		return errors.New(EMPTY_STRING)
	}

	g.name = name
	return nil
}

func (g *Group) Description() string {
	return g.description
}

func (g *Group) SetDescription(description string) {
	g.description = description
}
