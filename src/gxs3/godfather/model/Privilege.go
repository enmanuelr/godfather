package model

import "errors"

/*
 Privileges are used to determine what part of an applications users have access to. Privileges are assigned to groups.
 Any users that are members of that group implicitly inherit those privileges. A user can belong to more than one group.
 The user will inherit all privileges across all groups.
*/
type Privilege struct {
	*AbstractModel
	module      string //The module is like a classification or subdivision. Should correlate to an application module.
	key         string //Unique string identifier to the privilege
	name        string //Privilege name
	description string //Long form description that explain that the privilege provides access to
}

/*
 Creates a new privilege
*/
func NewPrivilege(id uint, module, key, name, description string) (*Privilege, error) {
	if len(module) < 1 || len(key) < 1 || len(name) < 1 {
		return nil, errors.New(EMPTY_STRING)
	}

	var p = Privilege{NewModel(id), module, key, name, description}

	return &p, nil
}

/*
 Returns a string representation of the privilege
*/
func (p *Privilege) String() string {
	return p.module
}

func (p *Privilege) Module() string {
	return p.module
}

func (p *Privilege) SetModule(module string) error {
	if len(module) < 1 {
		return errors.New(EMPTY_STRING)
	}

	p.module = module
	return nil
}

func (p *Privilege) Key() string {
	return p.key
}

func (p *Privilege) SetKey(key string) error {
	if len(key) < 1 {
		return errors.New(EMPTY_STRING)
	}

	p.key = key
	return nil
}

func (p *Privilege) Name() string {
	return p.name
}

func (p *Privilege) SetName(name string) error {
	if len(name) < 1 {
		return errors.New(EMPTY_STRING)
	}

	p.name = name
	return nil
}

func (p *Privilege) Description() string {
	return p.description
}

func (p *Privilege) SetDescription(description string) {
	p.description = description
}
