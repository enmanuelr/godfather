package model

import "errors"

const (
	INVALID_ARGUMENT    = "Unexpected argument value"
	NEGATIVE_ARGUMENT   = "Unexpected negative value"
	NIL_ARGUMENT        = "Unexpected nil value"
	EMPTY_STRING        = "Unexpected empty string"
	OUT_OF_RANGE        = "Argument falls outside of the expected range"
	INVALID_DATE_FORMAT = "The provided date is not in the expected format"
	BAD_VALUE           = "Bad value for field "

	DEFAULT_ID       = 0
	DATE_FORMAT      = "2006-01-02"
	TIME_FORMAT      = "15:04"
	DATE_TIME_FORMAT = DATE_FORMAT + " " + TIME_FORMAT
)

/*
 Base interface used to define models. All top level models will have an unsigned integer identifier tied to them. If it is a
 new model which has not been persisted yet, the ID value will be zero. It is recommended that implementations of the 
 toolkit implement this interface in all model definitions to keep a consistent structure working. Other parts of the 
 toolkit that work with models will often rely on these base elements.
*/
type Model interface {
	Id() uint
	SetId(id uint)
}

/*
 A base implementation of the Model interface. Provided to make it easier to implement the model interface in client model
 definitions
*/
type AbstractModel struct {
	id uint
}

/*
 A special type of model. When creating model relationships some elements are childs of others. In persisting and updating
 child elements, complexities arise. Therefore a uuid is placed on each child element. This is generated at run time, in
 the app, not when the model is persisted in the db. This simplifies a lot of the process of creating and updating child
 elements in the database.
*/
type ChildModel struct {
	*AbstractModel
	uuid string
}

/*
 Creates a new AbstractModel. This is usually used by other models which have embedded AbstractModel and need to initialize
 that property
*/
func NewModel(id uint) *AbstractModel {
	return &AbstractModel{id}
}

/*
 Creaes a new ChildModel. This is usually used by other models which have embedded ChildModel an dneed to initialize that
 property.
*/
func NewChildModel(id uint, uuid string) *ChildModel {
	return &ChildModel{NewModel(id), uuid}
}

/*
 Returns the integer identifier for the model. The ID should be unique for all models of the same type. If the model has
 yet to be persisted, it should have a value of zero in the ID.
*/
func (model *AbstractModel) Id() uint {
	return model.id
}

func (model *AbstractModel) SetId(id uint) {
	model.id = id
}

/*
 Returns the UUID for the child model. Child models have UUIDs to make operations of creation and updates to the database
 easier.
*/
func (c *ChildModel) Uuid() string {
	return c.uuid
}

func (c *ChildModel) SetUuid(uuid string) error {
	if len(uuid) < 1 {
		return errors.New(EMPTY_STRING)
	}

	c.uuid = uuid
	return nil
}
