/******************************** Initial Setup **********************************************/
delimiter ;
set foreign_key_checks = 0;
/*********************************************************************************************/

/********************************** creating tables *****************************************/
create table users (
  id_user int unsigned not null auto_increment,
  username varchar(32) not null,
  password varchar(60) not null,
  first_name varchar(32) not null,
  last_name varchar(32) not null,
  primary key (id_user),
  unique(username)
) engine=INNODB default charset=latin1;

create table groups (
  id_group int unsigned not null auto_increment,
  name varchar(32) not null,
  description text,
  primary key (id_group),
  unique(name)
) engine=INNODB default charset=latin1;

create table privileges (
  id_privilege int unsigned not null auto_increment,
  name varchar(32) not null,
  module varchar(32) not null,
  description text,
  primary key (id_privilege),
  unique(name)
) engine=INNODB default charset=latin1;

create table groups_privileges (
  id_group int unsigned not null,
  id_privilege int unsigned not null,
  primary key  (id_group, id_privilege),
  foreign key(id_group) references groups(id_group),
  foreign key(id_privilege) references privileges(id_privilege)
) engine=INNODB default charset=latin1;

create table users_groups (
  id_user int unsigned not null,
  id_group int unsigned not null,
  primary key  (id_user,id_group),
  foreign key(id_user) references users(id_user),
  foreign key(id_group) references groups(id_group)
) engine=INNODB default charset=latin1;


/********************************************************************************************/

/******************************** Populating Initial Data ***********************************/

drop procedure if exists __create_users;
delimiter //
create procedure __create_users()
begin
  declare _id_user int;
  declare _id_group int;

  insert into users(username, password, first_name, last_name)
  values('admin', '$2a$10$3B7Ru61jG.j8DC3hNGWFj.LGFxPyWyKemhWMCjD0bnCTRtKNfLtlC', 'Administrative', 'User');
  /*default password is 'admin' (no quotes). Hashed with bcrypt. Don't trust sha1, md5 and the others*/

  select id_user into _id_user
  from users 
  where username = 'admin';

  insert into groups(name, description) 
  values('ADMIN', 'Administrative Users');

  select id_group into _id_group
  from groups 
  where name = 'ADMIN';

  insert into users_groups(id_user, id_group)
  values(_id_user, _id_group);
end//
delimiter ;

call __create_users();
drop procedure __create_users;

insert into privileges(module, name, description) values
('godfather.login',                  'Login',                    'Login'),
('godfather.user-view',              'View Users',               'View Users'),
('godfather.user-add',               'Create User',              'Create New Users'),
('godfather.user-update',            'Edit Users',               'Edit Existing Users'),
('godfather.user-reset-password',    'Reset User Passwords',     'Reset passwords for local users'),
('godfather.user-remove',            'Remove Users',             'Remove Existing Users'),
('godfather.group-view',             'View Groups',              'View Groups. View users associated to groups.'),
('godfather.group-add',              'Create Groups',            'Create New Groups'),
('godfather.group-update',           'Edit Groups',              'Edit Existing Group Properties'),
('godfather.group-subscription',     'Group Subscription',       'Add and remove user membership to groups'),
('godfather.group-remove',           'Remove Groups',            'Remove Exsting Groups'),
('godfather.privilege-view',         'View Privileges',          'View Privileges Available'),
('godfather.privilege-grant',        'Grant Privilege',          'Grant Privilege to User Groups'),
('godfather.privilege-revoke',       'Revoke Privilege',         'Revoke Privilege from User Groups');

drop procedure if exists __assign_privs;
delimiter //

create procedure __assign_privs()
begin
	declare cont int;
  declare max int;
  declare _group int;
	set cont = 1;

  select count(1) + 1 into max
  from privileges;

  select id_group into _group
  from groups
  where name = 'ADMIN';
	
	while cont < max do
		insert into groups_privileges(id_group, id_privilege) 
		values(_group, cont);
		set cont = cont + 1;
	end while;
end //
delimiter ;

call __assign_privs();
drop procedure __assign_privs;

/********************************************************************************************/

/****************************************** cleanup *****************************************/
commit;
set foreign_key_checks = 1;
/********************************************************************************************/
